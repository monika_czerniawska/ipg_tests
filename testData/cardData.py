cards = {
    'visa': {
        'card_number': '4111111111111111',
        'month': '11',
        'year': '20',
        'cvv': '111',
        'name': 'Orson Card'
    },

    'masterCard': {
        'card_number': '5454545454545454',
        'month': '11',
        'year': '20',
        'cvv': '111',
        'name': 'Orson MasterCard'
    },

    'wrongMasterCard': {
        'card_number': '5454545454545455',
        'month': '11',
        'year': '20',
        'cvv': '111',
        'name': 'Orson MasterCard'
    }

}
