import datetime
import logging
import os

from dotenv import load_dotenv
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import pages
from utilities.logger import Logger

logging.basicConfig(
    filename='logs/logs-' + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M") + '.txt',
    level=logging.INFO,
    format="[%(levelname)-8s %(asctime)s] %(message)s"
)


def before_all(context):
    context.logger = Logger(context)
    context.logger.logger = logging.getLogger(__name__)
    load_dotenv('.env')
    context.base_e2e_url = os.getenv('E2E_URL')
    context.base_api_url = os.getenv('API_URL')

def before_feature(context, feature):
    context.feature_tags = feature.tags

def before_scenario(context, scenario):
    context.logger.current_scenario = scenario.name
    context.logger.info('Base E2E URL is: ' + context.base_e2e_url)
    context.logger.info('Base API URL is: ' + context.base_api_url)
    context.scenario_tags = scenario.tags
    if 'api' not in scenario.tags + context.feature_tags:
        opt = webdriver.ChromeOptions()
        opt.add_experimental_option('w3c', False)
        context.browser = webdriver.Remote(command_executor='http://localhost:4444/wd/hub',
            desired_capabilities=opt.to_capabilities())
        context.browser.set_window_size(os.getenv('SCREEN_WIDTH'), os.getenv('SCREEN_HEIGHT'))
        context.logger.info('Starting browser')
        context.pages = pages.Pages(context)


def before_step(context, step):
    context.logger.current_step = step.name


def after_scenario(context, scenario):
    if 'api' not in scenario.tags + context.feature_tags:
        context.logger.info('Closing browser')
        context.browser.quit()


def after_step(context, step):
    if step.status == 'failed' and 'api' not in context.scenario_tags + context.feature_tags:
        context.logger.debug('Taking screenshot of failed step')
        now = datetime.datetime.now()
        context.browser.save_screenshot(
            os.getcwd() + "/screenshots/" + now.strftime("%Y-%m-%d-%H-%M-") + context.scenario.name.replace(" ",
                                                                                                            "-") + ':' + step.name.replace(
                " ", "-") + ".png"
        )


def after_all(context):
    pass
