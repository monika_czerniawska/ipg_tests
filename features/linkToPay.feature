Feature: Link to pay

  @link_to_pay
  Scenario Outline: Create payment by link to pay
    Given user is logged in
    And user makes a ECOM payment in "Link To Pay"
    When user creates payment link for payment nr "<number_of_item>" "<name>" with price "<price>"
    And user sends email with link to pay to "ipgautotests@gmail.com"
    Then user logs in to email account
    And user could open an email and proceed the payment from link by "<card>"
  Examples:
    | number_of_item | name | price | card |
    |  1       | potatoes     | 12     | visa |
    |  1       | tomatoes     | 130    | masterCard |
    |  1       | lemons       | 678    | visa |