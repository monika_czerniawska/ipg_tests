Feature: Terminal online


  @terminal_online_new_customer
  Scenario Outline: Create payment with new customer
    Given user is logged in
    And user makes a MOTO payment in "Terminal Online"
    When user generates customer ID
    And user goes to payment
    And user makes "<type>" payment for an amount "<amount>" by "<card>"
    And user sends email transaction receipt to "ipgautotests@gmail.com"
#Gmail is temporary solution, to check if user open confirmation email we must check manually screenshot
    Then user logs in to email account
    And user could open an email with confirmation

  Examples:
    | type | amount | card |
    | Purchase | 10     | visa |
    | Purchase | 119999 | masterCard |
    | Purchase | 120000 | visa       |
    | Authorize only | 9999   | masterCard |
    | Authorize only | 129    | visa       |
    | Authorize only | 252    | masterCard |


  @terminal_online_form
  Scenario Outline: Fill form for new customer data
    Given user is logged in
    And user makes a MOTO payment in "Terminal Online"
    When user generates customer ID
    And user fill form by "<user>" data and set different "<address_type>" of address to "<status>"
    Then user should see completed fields of "<address_type>" address as "<result>"

  Examples:
    | user | address_type | status | result |
    | user1 | shipping |true | True  |
    | user1 | shipping | false | False |
    | user2 | billing | true | True |
    | user2 | billing | false | False |


