from behave import step

from pages import PagesType
from testData.credentials import boipa_user, gmail
from testData.userData import users


@step('user is logged in')
def step_impl(context: PagesType):
    context.pages.login_page.open()
    context.pages.login_page.validation_of_login_page_fields()
    context.pages.login_page.login_as(**boipa_user)
    context.pages.login_page.is_logged_user(boipa_user)


@step('user makes a MOTO payment in "{option}"')
def step_impl(context: PagesType, option):
    context.pages.base_menu_page.set_payment_option(option)
    context.pages.select_customer_type_page.validation_of_buttons()
    context.pages.select_customer_type_page.choose_new_customer()


@step('user generates customer ID')
def step_impl(context: PagesType):
    context.pages.take_payment_for_new_customer_page.validation_of_new_customer_form()
    context.pages.take_payment_for_new_customer_page.click_generate_customer_id_button()


@step('user goes to payment')
def step_impl(context: PagesType):
    context.pages.take_payment_for_new_customer_page.click_next_button()
    context.pages.customer_address_page.validation_of_address_forms()


@step('user makes "{type}" payment for an amount "{amount}" by "{card}"')
def step_impl(context: PagesType, type, amount, card):
    context.pages.customer_address_page.click_next_button()
    context.pages.customer_payment_detail_page.validation_of_payment_form()
    context.pages.customer_payment_detail_page.set__transaction_type(type)
    context.pages.customer_payment_detail_page.set_amount(amount)
    context.pages.customer_payment_detail_page.click_proceed_to_payment_button()
    context.pages.take_payment_for_new_customer_page.validation_of_iframe()
    context.pages.cashier_modes_page.pay_by(card)


@step('user sends email transaction receipt to "{email}"')
def step_impl(context: PagesType, email):
    context.pages.customer_transaction_result.validation_of_transaction_result_page()
    context.pages.customer_transaction_result.add_email(email)


@step('user logs in to email account')
def step_impl(context: PagesType):
    context.pages.login_gmail_page.login_to_gmail(**gmail)


@step('user could open an email with confirmation')
def step_impl(context: PagesType):
    context.pages.gmail_main_page.open_first_confirmation_email()


@step('user makes a ECOM payment in "{option}"')
def step_impl(context: PagesType, option):
    context.pages.base_menu_page.set_payment_option(option)


@step('user creates payment link for payment nr "{number_of_item}" "{name}" with price "{price}"')
def step_impl(context: PagesType, number_of_item, name, price):
    context.pages.link_to_pay_page.validation_of_l2p_form()
    context.pages.link_to_pay_page.fill_item_data(number_of_item, name, price)
    context.pages.link_to_pay_page.click_create_payment_link_button()
    context.pages.link_to_pay_page.validation_of_create_payment_elements()


@step('user sends email with link to pay to "{email}"')
def step_impl(context: PagesType, email):
    context.pages.link_to_pay_page.set_email(email)


@step('user could open an email and proceed the payment from link by "{card}"')
def step_impl(context: PagesType, card):
    context.pages.gmail_main_page.open_l2p_email()
    context.pages.l2p_email_page.open_link_from_email()
    context.pages.link_with_payment_page.validation_of_default_fields_in_l2p()
    context.pages.link_with_payment_page.choose_country('Irleand')
    context.pages.cashier_modes_page.pay_by(card)


@step('user fill form by "{user}" data and set different "{address_type}" of address to "{status}"')
def step_impl(context: PagesType, user, address_type, status):
    context.pages.take_payment_for_new_customer_page.validation_of_new_customer_form()
    context.pages.take_payment_for_new_customer_page.set_customer_data(**users[user])
    context.pages.take_payment_for_new_customer_page.click_next_button()
    context.pages.customer_address_page.validation_of_address_forms()
    context.pages.customer_address_page.customer_house_name.is_element_clickable()
    context.pages.customer_address_page.set_customer_address(**users[user]['customer_address'])
    context.pages.customer_address_page.set_different_address_type(
        user, address_type=address_type, address_status=status)


@step('user should see completed fields of "{address_type}" address as "{result}"')
def step_impl(context: PagesType, address_type, result):
    context.pages.customer_address_page.validation_of_active_element(result, address_type=address_type)
