from selenium.webdriver.common.by import By

from elements.button import Button
from elements.element import Element
from pageObjects.basePage import BasePage
from testData.cardData import cards


class CashierModes(BasePage):

    def __init__(self, context):
        super().__init__(context)
        # elements which displaying by default
        self.default_elements = []

        # elements which displaying with no cvv payment
        self.no_cvv_elements = []

        # payment via card
        self.card_number = Element(By.ID, 'cardNumber', context)
        self.default_elements.append(self.card_number)
        self.no_cvv_elements.append(self.card_number)

        self.valid_until_month = Element(By.ID, 'expiryMonth', context)
        self.default_elements.append(self.valid_until_month)
        self.no_cvv_elements.append(self.valid_until_month)

        self.valid_until_year = Element(By.ID, 'expiryYear', context)
        self.default_elements.append(self.valid_until_year)
        self.no_cvv_elements.append(self.valid_until_year)

        self.security_code_cvv = Element(By.ID, 'specinCreditCardCVV', context)
        self.default_elements.append(self.security_code_cvv)

        self.name_on_the_card = Element(By.ID, 'nameOnCard', context)
        self.default_elements.append(self.name_on_the_card)
        self.no_cvv_elements.append(self.name_on_the_card)

        self.pay = Button(By.CSS_SELECTOR, 'button.cc_submit.pair-right', context)
        self.default_elements.append(self.pay)
        self.no_cvv_elements.append(self.pay)

        self.cancel = Button(By.ID, 'cc_cancel', context)
        self.default_elements.append(self.cancel)
        self.no_cvv_elements.append(self.cancel)

        self.iframe = Element(By.ID, 'myriadIframe', context)

    def validation_of_default_payment_fields(self):
        self.assert_if_collection_of_elements_are_visible(self.default_elements)

    def validation_of_no_cvv_elements(self):
        self.assert_if_collection_of_elements_are_visible(self.no_cvv_elements)

    def set_card_details_and_pay(self, **kwargs):
        self.card_number.is_element_visible()
        self.card_number.value = kwargs['card_number']
        self.valid_until_month.value = kwargs['month']
        self.valid_until_year.value = kwargs['year']
        self.security_code_cvv.value = kwargs['cvv']
        self.name_on_the_card.value = kwargs['name']
        self.pay.is_element_clickable()
        self.pay.click()

    def pay_by(self, card):
        self.iframe.is_element_visible()
        self.switch_to_iframe_by_name('myriadIframe')
        self.validation_of_default_payment_fields()
        self.set_card_details_and_pay(**cards[card])
        self.switch_back_to_main_page()
