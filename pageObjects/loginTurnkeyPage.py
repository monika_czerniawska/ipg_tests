from selenium.webdriver.common.by import By

from elements.button import Button
from elements.dropdown import Dropdown
from elements.element import Element
from pageObjects.basePage import BasePage


class LoginPage(BasePage):

    URL = 'https://backofficeui-apiqa.test.myriadpayments.com'

    def __init__(self, context):
        super().__init__(context)
        self.default_eleemnts = []

        self.username_field = Element(By.NAME, 'username', context)
        self.default_eleemnts.append(self.username_field)

        self.password_field = Element(By.NAME, 'password', context)
        self.default_eleemnts.append(self.password_field)

        self.language_dropdown = Dropdown(By.ID, 'langPicker', context)
        self.default_eleemnts.append(self.language_dropdown)

        self.login_button = Button(By.ID, 'loginBtn', context)
        self.default_eleemnts.append(self.login_button)

        self.logged_as = Element(
            By.XPATH, '//*[text()="Logged in as:"]/following-sibling::h2', context)

    def validation_of_login_page_fields(self):
        self.assert_if_collection_of_elements_are_visible(self.default_eleemnts)

    def login_as(self, **kwargs):
        self.username_field.value = kwargs['username']
        self.password_field.value = kwargs['password']
        self.language_dropdown.value = kwargs['language']
        self.login_button.is_element_clickable()
        self.login_button.click()
    
    def is_logged_user(self, user):
        self.logged_as.is_element_visible()
        assert user['username'] in self.logged_as.text