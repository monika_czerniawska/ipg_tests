from selenium.webdriver.common.by import By

from elements.dropdown import Dropdown
from elements.element import Element
from pageObjects.basePage import BasePage


class BaseMenuPage(BasePage):
    URL = 'https://backofficeui-apiuat.test.myriadpayments.com'

    def __init__(self, context):
        super().__init__(context)
        self.transactions = Element(By.ID, 'transactions-list', context)
        self.take_payment = Dropdown(By.XPATH, '//span[contains(text(), \'{}\')]', context)
        self.void = Element(By.ID, 'void', context)
        self.capture = Element(By.ID, 'capture', context)
        self.refund = Dropdown(By.XPATH, '//span[contains(text(), \'{}\')]', context)
        self.select_reports = Dropdown(By.XPATH, '//span[contains(text(), \'{}\')]', context)
        self.instalments_plans = Element(By.ID, 'instalments-plans-list', context)
        self.recurring_payments = Element(By.ID, 'recurring-payments-list', context)
        self.settings = Element(By.ID, 'settings', context)
        self.help = Element(By.ID, 'help', context)
        self.logout = Element(By.ID, 'logout', context)

    def set_payment_option(self, option):
        self.take_payment.set_parameters('Take payment').click()
        self.take_payment.set_parameters(option).is_element_clickable()
        self.take_payment.set_parameters(option).click()

    def set_refund_option(self, option):
        self.refund.set_parameters('Refund').click()
        self.refund.set_parameters(option).click()

    def set_select_reports_option(self, option):
        self.select_reports.set_parameters('Select reports').click()
        self.select_reports.set_parameters(option).click()
