from selenium.webdriver.common.by import By

from elements.button import Button
from elements.dropdown import Dropdown
from elements.element import Element
from pageObjects.basePage import BasePage


class TakePaymentForNewCustomerPage(BasePage):

    def __init__(self, context):
        super().__init__(context)
        self.elements = []

        self.generate_customer_id = Button(
            By.CSS_SELECTOR, 'button.btn.btn-default.ng-binding', context)
        self.elements.append(self.generate_customer_id)

        self.customer_id = Element(By.XPATH, '//input[@name="customerId"]', context)
        self.elements.append(self.customer_id)

        self.first_name = Element(By.ID, 'customerFirstName', context)
        self.elements.append(self.first_name)

        self.middle_name = Element(By.ID, 'customerMiddleName', context)
        self.elements.append(self.middle_name)

        self.last_name = Element(By.ID, 'customerLastName', context)
        self.elements.append(self.last_name)

        self.document_type = Dropdown(By.ID, 'customerDocumentType', context)
        self.elements.append(self.document_type)

        self.document_number = Element(By.ID, 'customerDocumentNumber', context)
        self.elements.append(self.document_number)

        self.phone_number = Element(By.ID, 'customerPhone', context)
        self.elements.append(self.phone_number)

        self.email = Element(By.ID, 'customerEmail', context)
        self.elements.append(self.email)

        self.gender = Element(By.XPATH, '//label[contains(text(), \'{}\')]', context)

        self.male_button = Element(By.XPATH, '//label[contains(text(), "Male")]', context)
        self.elements.append(self.male_button)

        self.female_button = Element(By.XPATH, '//label[contains(text(), "Female")]', context)
        self.elements.append(self.female_button)

        # self.date_of_birth = Datepicker(By.ID, 'customerDateOfBirth', context)
        # self.elements.append(self.date_of_birth)

        self.next = Button(By.ID, 'step-2-next', context)
        self.elements.append(self.next)

        self.cancel = Button(By.ID, 'step-2-previous', context)
        self.elements.append(self.cancel)

        self.iframe = Element(
            By.XPATH, '//iframe', context)

    def validation_of_iframe(self):
        self.assert_if_element_is_visible(self.iframe)

    def validation_of_new_customer_form(self):
        self.assert_if_collection_of_elements_are_visible(self.elements)

    def set_gender(self, gender):
        self.gender.set_parameters(gender).click()

    def click_generate_customer_id_button(self):
        self.generate_customer_id.is_element_clickable()
        self.generate_customer_id.click()

    def click_next_button(self):
        self.next.is_element_clickable()
        self.next.click()

    def set_customer_data(self, **customer_data):
        self.first_name.value = customer_data['first_name']
        self.middle_name.value = customer_data['middle_name']
        self.last_name.value = customer_data['last_name']
        self.document_type.value = customer_data['document_type']
        self.document_number.value = customer_data['document_number']
        self.phone_number.value = customer_data['phone_number']
        self.email.value = customer_data['email']
        self.set_gender(customer_data['gender'])
        #self.date_of_birth.set_date(customer_data['date_of_birth'])