from selenium.webdriver.common.by import By

from elements.button import Button
from pageObjects.basePage import BasePage


class SelectCustomerTypePage(BasePage):

    def __init__(self, context):
        super().__init__(context)
        self.buttons = []

        self.new_customer = Button(By.ID, 'newCustomerButton', context)
        self.buttons.append(self.new_customer)

        self.existing_customer = Button(By.ID, 'existingCustomerButton', context)
        self.buttons.append(self.existing_customer)

    def validation_of_buttons(self):
        self.assert_if_collection_of_elements_are_visible(self.buttons)

    def choose_new_customer(self):
        self.new_customer.is_element_clickable()
        self.new_customer.click()