from selenium.webdriver.common.by import By

from elements.button import Button
from elements.dropdown import Dropdown
from elements.element import Element
from pageObjects.basePage import BasePage


class CustomerPaymentDetailsPage(BasePage):

    def __init__(self, context):
        super().__init__(context)
        self.elements = []

        # Payee section
        self.payee_is_same_as_customer = Button(By.ID, 'useCustomerAsPayer-true', context)
        self.elements.append(self.payee_is_same_as_customer)

        self.payee_is_not_same_as_customer = Button(By.ID, 'useCustomerAsPayer-false', context)
        self.elements.append(self.payee_is_not_same_as_customer)

        # Payment Amount & Currency
        self.merchant_reference = Element(By.ID, 'merchantReference', context)
        self.elements.append(self.merchant_reference)

        self.transaction_type = Dropdown(By.ID, 'trx-type', context)
        self.elements.append(self.transaction_type)

        self.currency = Dropdown(By.ID, 'currency', context)
        self.elements.append(self.currency)

        self.bank_mid = Dropdown(By.ID, 'bankMid', context)
        self.elements.append(self.bank_mid)

        self.without_cvv_true = Button(By.ID, 'specinProcessWithoutCvv2-true', context)
        self.elements.append(self.without_cvv_true)

        self.without_cvv_false = Button(By.ID, 'specinProcessWithoutCvv2-false', context)
        self.elements.append(self.without_cvv_false)

        self.order_amount = Element(By.ID, 'orderAmount', context)
        self.elements.append(self.order_amount)

        self.shipping_amount = Element(By.ID, 'shipping-amount', context)
        self.elements.append(self.shipping_amount)

        self.tax_amount = Element(By.ID, 'tax-amount', context)
        self.elements.append(self.tax_amount)

        self.provide_discount = Element(By.XPATH, '//input[@name="provide-discount"]', context)
        self.elements.append(self.provide_discount)

        self.total_amount = Element(By.ID, 'total-amount', context)
        self.elements.append(self.total_amount)

        self.proceed_to_payment = Button(By.ID, 'proceed-to-payment', context)
        self.elements.append(self.proceed_to_payment)

    def validation_of_payment_form(self):
        self.assert_if_collection_of_elements_are_visible(self.elements)

    def set_amount(self, amount):
        self.order_amount.is_element_visible()
        self.order_amount.value = amount

    def set__transaction_type(self, transaction_type):
        self.transaction_type.value = transaction_type

    def click_proceed_to_payment_button(self):
        self.proceed_to_payment.is_element_clickable()
        self.proceed_to_payment.click()


