from hamcrest import assert_that, equal_to
from selenium.webdriver.common.by import By

from elements.button import Button
from elements.dropdown import Dropdown
from elements.element import Element
from pageObjects.basePage import BasePage
from testData.userData import users


class CustomerAddressPage(BasePage):

    def __init__(self, context):
        super().__init__(context)
        self.elements = []
        self.shipping_address_elements = []
        self.billing_address_elements = []

        # Customer Address section
        self.customer_house_name = Element(By.ID, 'customerAddressHouseName', context)
        self.elements.append(self.customer_house_name)

        self.customer_flat_number = Element(By.ID, 'customerAddressFlat', context)
        self.elements.append(self.customer_flat_number)

        self.customer_city = Element(By.ID, 'customerAddressCity', context)
        self.elements.append(self.customer_city)

        self.customer_post_code = Element(By.ID, 'customerAddressPostalCode', context)
        self.elements.append(self.customer_post_code)

        self.customer_country = Dropdown(By.ID, 'customerAddressCountry', context)
        self.elements.append(self.customer_country)

        self.customer_house_number = Element(By.ID, 'customerAddressHouseNumber', context)
        self.elements.append(self.customer_house_number)

        self.customer_street = Element(By.ID, 'customerAddressStreet', context)
        self.elements.append(self.customer_street)

        self.customer_district = Element(By.ID, 'customerAddressDistrict', context)
        self.elements.append(self.customer_district)

        self.customer_state = Element(By.ID, 'customerAddressState', context)
        self.elements.append(self.customer_state)

        self.customer_phone = Element(By.ID, 'customerAddressPhone', context)
        self.elements.append(self.customer_phone)

        # Shipping Address
        self.shipping_house_name = Element(By.ID, 'customerShippingAddressHouseName', context)
        self.elements.append(self.shipping_house_name)
        self.shipping_address_elements.append(self.shipping_house_name)

        self.shipping_flat_number = Element(By.ID, 'customerShippingAddressFlat', context)
        self.elements.append(self.shipping_flat_number)
        self.shipping_address_elements.append(self.shipping_flat_number)

        self.shipping_city = Element(By.ID, 'customerShippingAddressCity', context)
        self.elements.append(self.shipping_city)
        self.shipping_address_elements.append(self.shipping_city)

        self.shipping_post_code = Element(By.ID, 'customerShippingAddressPostalCode', context)
        self.elements.append(self.shipping_post_code)
        self.shipping_address_elements.append(self.shipping_post_code)

        self.shipping_country = Dropdown(By.ID, 'customerShippingAddressCountry', context)
        self.elements.append(self.shipping_country)
        self.shipping_address_elements.append(self.shipping_country)

        self.shipping_house_number = Element(By.ID, 'customerShippingAddressHouseNumber', context)
        self.elements.append(self.shipping_house_number)
        self.shipping_address_elements.append(self.shipping_house_number)

        self.shipping_street = Element(By.ID, 'customerShippingAddressStreet', context)
        self.elements.append(self.shipping_street)
        self.shipping_address_elements.append(self.shipping_street)

        self.shipping_district = Element(By.ID, 'customerShippingAddressDistrict', context)
        self.elements.append(self.shipping_district)
        self.shipping_address_elements.append(self.shipping_district)

        self.shipping_state = Element(By.ID, 'customerShippingAddressState', context)
        self.elements.append(self.shipping_state)
        self.shipping_address_elements.append(self.shipping_state)

        self.shipping_phone = Element(By.ID, 'customerShippingAddressPhone', context)
        self.elements.append(self.shipping_phone)
        self.shipping_address_elements.append(self.shipping_phone)

        # Billing Address
        self.billing_house_name = Element(By.ID, 'customerBillingAddressHouseName', context)
        self.elements.append(self.billing_house_name)
        self.billing_address_elements.append(self.billing_house_name)

        self.billing_flat_number = Element(By.ID, 'customerBillingAddressFlat', context)
        self.elements.append(self.billing_flat_number)
        self.billing_address_elements.append(self.billing_flat_number)

        self.billing_city = Element(By.ID, 'customerBillingAddressCity', context)
        self.elements.append(self.billing_city)
        self.billing_address_elements.append(self.billing_city)

        self.billing_post_code = Element(By.ID, 'customerBillingAddressPostalCode', context)
        self.elements.append(self.billing_post_code)
        self.billing_address_elements.append(self.billing_post_code)

        self.billing_country = Dropdown(By.ID, 'customerBillingAddressCountry', context)
        self.elements.append(self.billing_country)
        self.billing_address_elements.append(self.billing_country)

        self.billing_house_number = Element(By.ID, 'customerBillingAddressHouseNumber', context)
        self.elements.append(self.billing_house_number)
        self.billing_address_elements.append(self.billing_house_number)

        self.billing_street = Element(By.ID, 'customerBillingAddressStreet', context)
        self.elements.append(self.billing_street)
        self.billing_address_elements.append(self.billing_street)

        self.billing_district = Element(By.ID, 'customerBillingAddressDistrict', context)
        self.elements.append(self.billing_district)
        self.billing_address_elements.append(self.billing_district)

        self.billing_state = Element(By.ID, 'customerBillingAddressState', context)
        self.elements.append(self.billing_state)
        self.billing_address_elements.append(self.billing_state)

        self.billing_phone = Element(By.ID, 'customerBillingAddressPhone', context)
        self.elements.append(self.billing_phone)
        self.billing_address_elements.append(self.billing_phone)

        self.different_shipping_address_true = Button(By.ID, 'useCustomerAddressAsShippingAddress-true', context)
        self.elements.append(self.different_shipping_address_true)

        self.different_shipping_address_false = Button(By.ID, 'useCustomerAddressAsShippingAddress-false', context)
        self.elements.append(self.different_shipping_address_false)

        self.different_billing_address_true = Button(By.ID, 'useCustomerAddressAsBillingAddress-true', context)
        self.elements.append(self.different_billing_address_true)

        self.different_billing_address_false = Button(By.ID, 'useCustomerAddressAsBillingAddress-false', context)
        self.elements.append(self.different_billing_address_false)

        self.previous = Button(By.ID, 'previusBtn', context)
        self.elements.append(self.previous)

        self.next = Button(By.ID, 'step-3-next', context)
        self.elements.append(self.next)

        self.logger = context.logger

    def validation_of_address_forms(self):
        self.assert_if_collection_of_elements_are_visible(self.elements)

    def validation_of_active_element(self, result, address_type='shipping'):
        element = getattr(self, f'{address_type}_house_name', None)

        if result == 'True':
            assert_that(str(element.is_element_clickable()), equal_to(result))
        else:
            assert_that(str(element.is_element_clickable()), equal_to(result))

    def click_next_button(self):
        self.next.is_element_clickable()
        self.next.click()

    def set_customer_address(self, **address):
        self.customer_house_name.is_element_clickable()
        self.customer_house_name.value = address['house_name']
        self.customer_house_number.value = address['house_number']
        self.customer_flat_number.value = address['flat_number']
        self.customer_street.value = address['street_name']
        self.customer_city.value = address['city']
        self.customer_district.value = address['district']
        self.customer_post_code.value = address['post_code']
        self.customer_state = address['state']
        self.customer_country.value = address['country']
        self.customer_phone.value = address['phone']

    def set_shipping_address(self, **address):
        self.shipping_house_name.value = address['house_name']
        self.shipping_house_number.value = address['house_number']
        self.shipping_flat_number.value = address['flat_number']
        self.shipping_street.value = address['street_name']
        self.shipping_city.value = address['city']
        self.shipping_district.value = address['district']
        self.shipping_post_code.value = address['post_code']
        self.shipping_state.value = address['state']
        self.shipping_country.value = address['country']
        self.shipping_phone.value = address['phone']

    def set_billing_address(self, **address):
        self.billing_house_name.value = address['house_name']
        self.billing_house_number.value = address['house_number']
        self.billing_flat_number.value = address['flat_number']
        self.billing_street.value = address['street_name']
        self.billing_city.value = address['city']
        self.billing_district.value = address['district']
        self.billing_post_code.value = address['post_code']
        self.billing_state.value = address['state']
        self.billing_country.value = address['country']
        self.billing_phone.value = address['phone']

    def fill_customer_address_fields(self, user):
        user_address = users[user]['customer_address']
        self.set_customer_address(**user_address)

    def set_different_address_type(self, user, address_status='true', address_type='shipping'):
        """
        Value of address_status parameter should be set to 'false', if address isn't different.
        Value of address_type parameter should be set to 'shipping' or 'billing'


        :type address_status: str
        :type address_type: str
        """
        if address_type == 'shipping':
            if address_status == 'true':
                self.different_shipping_address_false.click()
                self.assert_if_collection_of_elements_are_clickable(self.shipping_address_elements)
                self.set_shipping_address(**users[user]['shipping_address'])
        elif address_type == 'billing':
            if address_status == 'true':
                self.different_billing_address_false.click()
                self.assert_if_collection_of_elements_are_clickable(self.billing_address_elements)
                self.set_billing_address(**users[user]['billing_address'])
