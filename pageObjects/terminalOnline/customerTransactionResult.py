from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from elements.element import Element
from pageObjects.basePage import BasePage


class CustomerTransactionResult(BasePage):

    def __init__(self, context):
        super().__init__(context)
        self.elements = []

        self.email = Element(By.ID, 'transactionReceiptEmail', context)
        self.elements.append(self.email)

    def validation_of_transaction_result_page(self):
        self.assert_if_collection_of_elements_are_visible(self.elements)

    def add_email(self, email):
        self.email.is_element_clickable()
        self.email.value_key_action(email, Keys.ENTER)
