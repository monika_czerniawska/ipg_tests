from selenium.webdriver.common.by import By

from elements.element import Element
from pageObjects.basePage import BasePage


class GmailMain(BasePage):

    def __init__(self, context):
        super().__init__(context)
        self.confirmation_email_title = Element(
            By.XPATH, '//tr[1][contains(.,"Transaction Receipt for Merchant BOIPA\'s MERCHANT")]', context)
        self.l2p_email_title = Element(
            By.XPATH, '//tr[1][contains(.,"Payment requirement")]', context)

    def open_first_confirmation_email(self):
        self.confirmation_email_title.is_element_clickable()
        self.confirmation_email_title.click()

    def open_l2p_email(self):
        self.l2p_email_title.is_element_clickable()
        self.l2p_email_title.click()