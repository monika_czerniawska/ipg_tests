from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from elements.element import Element
from pageObjects.basePage import BasePage


class LoginGmail(BasePage):
    URL = 'https://gmail.com'

    def __init__(self, context):
        super().__init__(context)
        self.default_elements = []

        self.email = Element(By.ID, 'identifierId', context)
        self.default_elements.append(self.email)

        self.password = Element(By.NAME, 'password', context)
        self.default_elements.append(self.password)

    def validation_login_fields(self):
        self.assert_if_collection_of_elements_are_visible(self.default_elements)

    def pass_login(self, address):
        self.email.value_key_action(address, Keys.ENTER)

    def pass_password(self, password):
        self.password.value_key_action(password, Keys.ENTER)

    def login_to_gmail(self, **kwargs):
        self.open()
        self.pass_login(kwargs['login'])
        self.pass_password(kwargs['password'])
