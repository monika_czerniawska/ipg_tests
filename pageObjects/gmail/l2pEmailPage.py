import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

from elements.element import Element
from pageObjects.basePage import BasePage


class L2pEmailPage(BasePage):

    def __init__(self, context):
        super().__init__(context)
        self.transaction_link = Element(
            By.LINK_TEXT, 'Click Here to Complete your Payment', context)

    def open_link_from_email(self, timeout=15, number_of_card=2, card_number=1):
        """Opens link, check if in browser is required number of cards and switch to chosen card

        :param timeout: int
        :param number_of_card: int
        :param card_number: int
        """
        self.transaction_link.is_element_clickable(timeout)
        self.transaction_link.click()
        WebDriverWait(self.context.browser, timeout).until(EC.number_of_windows_to_be(number_of_card))
        self.context.browser.switch_to.window(self.context.browser.window_handles[card_number])
