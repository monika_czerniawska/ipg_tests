from selenium.webdriver.common.by import By

from elements.button import Button
from elements.dropdown import Dropdown
from elements.element import Element
from pageObjects.basePage import BasePage


class LinkWithPaymentPage(BasePage):

    def __init__(self, context):
        super().__init__(context)
        self.default_elements = []

        self.required_receipt = Element(By.ID, 'doNotSendReceiptCopy', context)
        self.default_elements.append(self.required_receipt)

        self.country_select = Dropdown(By.TAG_NAME, 'select', context)
        self.default_elements.append(self.country_select)

        self.proceed = Button(By.ID, 'submit', context)
        self.default_elements.append(self.proceed)

    def validation_of_default_fields_in_l2p(self):
        self.assert_if_collection_of_elements_are_visible(self.default_elements)

    def choose_country(self, country):
        self.country_select.is_element_clickable()
        self.country_select.value = country
        self.proceed.is_element_clickable()
        self.proceed.click()
