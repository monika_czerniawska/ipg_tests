from selenium.webdriver.common.by import By

from elements.button import Button
from elements.datepicker import Datepicker
from elements.dropdown import Dropdown
from elements.element import Element
from pageObjects.basePage import BasePage


class LinkToPayPage(BasePage):

    def __init__(self, context):
        super().__init__(context)
        #list of all default elements on the page
        self.default_elements = []
        #list of elements which occurs after create payment action
        self.create_payment_elements = []

        # Company details
        self.customer_service_email = Element(By.ID, 'contactEmail', context)
        self.default_elements.append(self.customer_service_email)

        self.customer_service_telephone = Element(By.ID, 'contactPhone', context)
        self.default_elements.append(self.customer_service_telephone)

        self.other_contact_details = Element(By.ID, 'otherContact', context)
        self.default_elements.append(self.other_contact_details)

        self.save_company_details_as_default = Button(By.ID, 'save-as-default', context)
        self.default_elements.append(self.save_company_details_as_default)

        # Link page details
        self.page_title = Element(By.ID, 'pageHeader', context)
        self.default_elements.append(self.page_title)

        self.welcome_message = Element(By.ID, 'welcomeMessage', context)
        self.default_elements.append(self.welcome_message)

        self.thank_you_message = Element(By.ID, 'thankYouMessage', context)
        self.default_elements.append(self.thank_you_message)

        self.save_page_details_as_default = Button(By.ID, 'page-details-save-as-default', context)
        self.default_elements.append(self.save_page_details_as_default)

        # Link details
        self.order_id = Element(By.ID, 'orderId', context)
        self.default_elements.append(self.order_id)

        self.customer_id = Element(By.ID, 'customerId', context)
        self.default_elements.append(self.customer_id)

        self.expiry_date = Datepicker(By.ID, 'date-range-calendar', context)
        self.default_elements.append(self.expiry_date)

        self.transaction_type = Dropdown(By.XPATH, '//select[@id="ltp-transaction-type"]', context)
        self.default_elements.append(self.transaction_type)

        # Products
        self.add_item = Button(By.ID, 'ltp-add-item', context)
        self.default_elements.append(self.add_item)

        self.item_name = Element(By.ID, 'ltp-{}-item-name', context)

        self.item_1_name = Element(By.ID, 'ltp-1-item-name', context)
        self.default_elements.append(self.item_1_name)

        self.unit_price = Element(By.ID, 'ltp-{}-item-unit-price', context)

        self.unit_1_price = Element(By.ID, 'ltp-1-item-unit-price', context)
        self.default_elements.append(self.unit_1_price)

        self.create_payment_link = Button(By.ID, 'createPaymentBtn', context)

        self.to_email = Element(By.ID, 'targetEmail', context)

        self.send_by_email = Button(By.ID, 'sendByEmailBtn', context)

    def validation_of_l2p_form(self):
        self.assert_if_collection_of_elements_are_visible(self.default_elements)

    def validation_of_create_payment_elements(self):
        self.assert_if_collection_of_elements_are_visible(self.create_payment_elements)

    def fill_item_data(self, number_of_item, name, price):
        self.item_name.set_parameters(number_of_item)
        self.unit_price.set_parameters(number_of_item)
        self.item_name.is_element_clickable()
        self.unit_price.is_element_clickable()
        self.item_name.value = name
        self.unit_price.value = price

    def click_create_payment_link_button(self):
        self.create_payment_link.is_element_clickable()
        self.create_payment_link.click()

    def set_email(self, email):
        self.to_email.is_element_clickable()
        self.to_email.value = email
        self.send_by_email.is_element_clickable()
        self.send_by_email.click()
