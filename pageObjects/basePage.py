from hamcrest import assert_that, equal_to


class BasePage:
    URL = ''

    def __init__(self, context):
        self.context = context

    def open(self):
        self.context.browser.get(self.context.base_e2e_url + self.URL)

    def switch_to_iframe_by_name(self, name):
        self.context.browser.switch_to.frame(name)

    def switch_back_to_main_page(self):
        self.context.browser.switch_to.default_content()

    def assert_if_element_is_visible(self, element):
        assert_that(element.is_element_visible(), equal_to(True))

    def assert_if_collection_of_elements_are_visible(self, elements):
        for element in elements:
            assert_that(
                element.is_element_visible(), equal_to(True), f'{element.__str__()} isn\'t visible')

    def assert_if_collection_of_elements_are_clickable(self, elements):
        for element in elements:
            assert_that(
                element.is_element_clickable(), equal_to(True), f'{element.__str__()} isn\'t clickable')

    def assert_if_collection_of_elements_are_not_clickable(self, elements):
        for element in elements:
            assert_that(
                element.is_element_clickable(), equal_to(False), f'{element.__str__()} is clickable')
