import pageObjects.baseMenuPage as baseMenuPage
import pageObjects.basePage as basePage
import pageObjects.cashierUi.cashierModesPage as cashierModesPage
import pageObjects.gmail.gmailMainPage as gmailMainPage
import pageObjects.gmail.l2pEmailPage as l2pEmailPage
import pageObjects.gmail.loginGmailPage as loginGmailPage
import pageObjects.linkToPay.linkToPayPage as linkToPayPage
import pageObjects.linkToPay.linkWithPaymentPage as linkWithPaymentPage
import pageObjects.loginTurnkeyPage as loginTurnkeyPage
import pageObjects.terminalOnline.customerAddressPage as customerAddressPage
import pageObjects.terminalOnline.customerPaymentDetailsPage as customerPaymentDetailsPage
import pageObjects.terminalOnline.customerTransactionResult as customerTransactionResult
import pageObjects.terminalOnline.selectCustomerTypePage as selectCustomerTypePage
import pageObjects.terminalOnline.takePaymentForNewCustomerPage as takePaymentForNewCustomerPage


class Pages:
    login_page = None  # type: loginTurnkeyPage.LoginPage
    base_page = None  # type: basePage.BasePage
    base_menu_page = None  # type: baseMenuPage.BaseMenuPage
    customer_address_page = None  # type: customerAddressPage
    customer_payment_detail_page = None  # type: customerPaymentDetailsPage
    select_customer_type_page = None  # type: selectCustomerTypePage
    take_payment_for_new_customer_page = None  # type: takePaymentForNewCustomerPage
    cashier_modes_page = None  # type: cashierModesPage
    customer_transaction_result = None  # type: customerTransactionResult
    login_gmail_page = None  # type: loginGmailPage
    gmail_main_page = None  # type:  gmailMainPage
    l2p_email_page = None  # type: l2pEmailPage
    link_to_pay_page = None  # type: linkToPayPage
    link_with_payment_page = None  #type: linkWithPaymentPage

    def __init__(self, context):
        self.login_page = loginTurnkeyPage.LoginPage(context)
        self.base_page = basePage.BasePage(context)
        self.base_menu_page = baseMenuPage.BaseMenuPage(context)
        self.customer_address_page = customerAddressPage.CustomerAddressPage(context)
        self.customer_payment_detail_page = customerPaymentDetailsPage.CustomerPaymentDetailsPage(context)
        self.select_customer_type_page = selectCustomerTypePage.SelectCustomerTypePage(context)
        self.take_payment_for_new_customer_page = takePaymentForNewCustomerPage.TakePaymentForNewCustomerPage(context)
        self.cashier_modes_page = cashierModesPage.CashierModes(context)
        self.customer_transaction_result = customerTransactionResult.CustomerTransactionResult(context)
        self.login_gmail_page = loginGmailPage.LoginGmail(context)
        self.gmail_main_page = gmailMainPage.GmailMain(context)
        self.l2p_email_page = l2pEmailPage.L2pEmailPage(context)
        self.link_to_pay_page = linkToPayPage.LinkToPayPage(context)
        self.link_with_payment_page = linkWithPaymentPage.LinkWithPaymentPage(context)


class PagesType:
    pages: Pages
